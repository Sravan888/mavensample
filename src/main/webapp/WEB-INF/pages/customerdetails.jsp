<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h2>Customer Details</h2>
	<table>
	<tr><td>Customer Code</td> <td> ${Customer.customerCode }</td></tr>
	<tr><td>Customer Name</td> <td> ${Customer.customerName }</td></tr>
	<tr><td>Address</td> <td> ${Customer.address }</td></tr>
	<tr><td>Contact No</td> <td> ${Customer.contactno }</td></tr>
	</table>
</body>
</html>