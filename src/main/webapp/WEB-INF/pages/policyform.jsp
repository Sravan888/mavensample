<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Customer Form</title>
</head>
<body>

<h2> Issue New Policy Form</h2>
<form action="issuePolicy" method="post">
	<p><b>Customer Details Entry Form</b></p>
	<table>
	<tr><td>Customer Code</td> <td><input type="text" name="customerCode"/></td></tr>
	<tr><td>Customer Name</td> <td><input type="text" name="customerName"/></td></tr>
	<tr><td>Address</td> <td><input type="text" name="address"/></td></tr>
	<tr><td>Contact No</td> <td><input type="text" name="contactno"/></td></tr>
	</table>
	
	<p> <b>Policy Details Entry Form</b></p>
	<table>
	<tr><td>Policyno</td> <td><input type="text" name="policyno"/></td></tr>
	<tr><td>Premium</td> <td><input type="text" name="premium"/></td></tr>
	<tr><td>Policy Mode</td> <td><input type="text" name="policyMode"/></td></tr>
	<tr><td>Startdate[dd-MM-yyyy]</td> <td><input type="text" name="startDate"/></td></tr>
	</table> <br/>
	<input type="submit" value="Issue Policy to Customer"/>
	
</form>
</body>
</html>