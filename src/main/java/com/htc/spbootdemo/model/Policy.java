package com.htc.spbootdemo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="policy")
public class Policy {

	@Id
	private long policyno;
	private double premium;
	@Column(name="POLICYMODE")
	private String policyMode;
	
	@Column(name="STARTDATE")
	@Temporal(TemporalType.DATE)
	private Date startDate;
	
	@ManyToOne
	@JoinColumn(name="CUSTOMERCODE")
	Customer customer;
	
	
	public Policy() {}
	public Policy(long policyno, double premium, String policyMode, Date startDate) {
		super();
		this.policyno = policyno;
		this.premium = premium;
		this.policyMode = policyMode;
		this.startDate = startDate;
	}
	public long getPolicyno() {
		return policyno;
	}
	public void setPolicyno(long policyno) {
		this.policyno = policyno;
	}
	public double getPremium() {
		return premium;
	}
	public void setPremium(double premium) {
		this.premium = premium;
	}
	public String getPolicyMode() {
		return policyMode;
	}
	public void setPolicyMode(String policyMode) {
		this.policyMode = policyMode;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	@Override
	public String toString() {
		return "Policy [policyno=" + policyno + ", premium=" + premium + ", policyMode=" + policyMode + ", startDate="
				+ startDate + ", customer=" + customer + "]";
	}
	
	
}
