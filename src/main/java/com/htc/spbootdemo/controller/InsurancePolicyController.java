package com.htc.spbootdemo.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.htc.spbootdemo.services.InsuranceService;
import com.htc.spbootdemo.to.CustomerPolicyTO;
import com.htc.spbootdemo.to.CustomerTO;
import com.htc.spbootdemo.to.PolicyTO;

@Controller
public class InsurancePolicyController {

	@Autowired
	InsuranceService insuranceService;
	
	
	@RequestMapping(value="/customerForm", method=RequestMethod.GET)
	public String showCustomerForm() {
		return "customerform";
	}
	
	@PostMapping("/addCustomer")    //spring 4.x onwards
	public String addCustomer(@ModelAttribute(name="customer") @Valid CustomerTO customerTO, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
//		if(bindingResult.hasErrors()) {
//			return "customerform";
//		}
		System.out.println(customerTO);
		boolean result = insuranceService.addCustomer(customerTO);
		if(result) {
			redirectAttributes.addFlashAttribute("customerName", customerTO.getCustomerName());
			redirectAttributes.addFlashAttribute("msg", "Added successfully");
			return "redirect:/addCustomerSuccess";
		}
		else
			return "customerform";
	}
	
	@GetMapping("/addCustomerSuccess")
	public String addCustomerSuccess() {
		return "customeraddsuccess";
	}
	
	@RequestMapping(value="/issuePolicyForm", method=RequestMethod.GET)
	public String issuePolicyToCustomerForm() {
		return "policyform";
	}
	
	@RequestMapping(value="/issuePolicy", method=RequestMethod.POST)
	public String issuePolicyToCustomer(@ModelAttribute CustomerPolicyTO customerPolicyTO,  RedirectAttributes attributes) {
		
		System.out.println(customerPolicyTO);
		CustomerTO customerTO = new CustomerTO(customerPolicyTO.getCustomerCode(), customerPolicyTO.getCustomerName(), customerPolicyTO.getAddress(), customerPolicyTO.getContactno());;
		PolicyTO policyTO = new PolicyTO(customerPolicyTO.getPolicyno(), customerPolicyTO.getPremium(), customerPolicyTO.getPolicyMode(), customerPolicyTO.getStartDate());;
		
		System.out.println(customerTO);
		System.out.println(policyTO);
		
		boolean result = insuranceService.addCustomerWithPolicy(customerTO, policyTO);
		
		if(result) {
			attributes.addFlashAttribute("policyno", customerPolicyTO.getPolicyno());
			attributes.addFlashAttribute("customerName", customerPolicyTO.getCustomerName());
			return "redirect:/issuePolicySuccess";
		}
		else 
			return "policyform";
	}
	
	@RequestMapping(value="/issuePolicySuccess", method=RequestMethod.GET)
	public String issuePolicySuccess() {
		return "issuepolicysuccess";
	}
	
	@GetMapping("/searchPolicyForm")
	public String searchPolicyForm() {
		return "searchpolicyform";
	}
	
	@PostMapping("/getPolicyInfo")
	public ModelAndView getPolicyDetails(@RequestParam(name="policyno") long policyno) {
		
		ModelAndView mv = new ModelAndView();
		PolicyTO policyTO = insuranceService.getInsurancePolicy(policyno);
		if(policyTO == null) {
			mv.setViewName("policydetailsmissing");		
			mv.addObject("policyno", policyno);
		}
		else {
			mv.setViewName("policydetails");
			mv.addObject("policyTO", policyTO);
		}
		return mv;
	}
/*	
	@ExceptionHandler
	public ModelAndView execeptionHandler(Exception ex) {
		ModelAndView mv = new ModelAndView("error", "errorMsg", ex.toString());
		return mv;
	}
*/	
}
