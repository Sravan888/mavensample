package com.htc.spbootdemo.services;

import com.htc.spbootdemo.model.Customer;
import com.htc.spbootdemo.to.CustomerTO;
import com.htc.spbootdemo.to.PolicyTO;

public interface InsuranceService {
	
	public boolean addCustomer(CustomerTO customer);
	public boolean addCustomerWithPolicy(CustomerTO customerTO, PolicyTO policyTO);
	public boolean addPolicy(PolicyTO policy);
	//public boolean addPolicyToCustomer(PolicyTO policyTO);
	
	//public List<PolicyTO> getInsurancePolicies(String customerCode);
	public PolicyTO getInsurancePolicy(long policyno);
	public Customer getCustomerDetails(long policyno);
}
