package com.htc.spbootdemo.services;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.htc.spbootdemo.model.Customer;
import com.htc.spbootdemo.model.Policy;
import com.htc.spbootdemo.repository.CustomerRepository;
import com.htc.spbootdemo.repository.PolicyRepository;
import com.htc.spbootdemo.to.CustomerTO;
import com.htc.spbootdemo.to.PolicyTO;

@Service
public class InsuranceServiceImpl implements InsuranceService {

	@Autowired
	CustomerRepository customerRepository;
	
	@Autowired
	PolicyRepository policyRepository;
	
	private CustomerTO getCustomerTO(Customer customer) {
		CustomerTO customerTO = new CustomerTO();
		customerTO.setCustomerCode(customer.getCustomerCode());
		customerTO.setCustomerName(customer.getCustomerName());
		customerTO.setAddress(customer.getAddress());
		customerTO.setContactno(customer.getContactno());
		return customerTO;
	}
	private Customer getCustomer(CustomerTO customerTO) {
		Customer customer= new Customer();
		customer.setCustomerCode(customerTO.getCustomerCode());
		customer.setCustomerName(customerTO.getCustomerName());
		customer.setAddress(customerTO.getAddress());
		customer.setContactno(customerTO.getContactno());
		return customer;
	}
	
	private Policy getPolicy(PolicyTO policyTO) {
		Policy policy = new Policy();
		policy.setPolicyno(policyTO.getPolicyno());
		policy.setPremium(policyTO.getPremium());
		policy.setPolicyMode(policyTO.getPolicyMode());
		policy.setStartDate(policyTO.getStartDate());
		return policy;
	}
	
	private PolicyTO getPolicyTO(Policy policy) {
		PolicyTO policyTO =new PolicyTO();
		policyTO.setPolicyno(policy.getPolicyno());
		policyTO.setPremium(policy.getPremium());
		policyTO.setPolicyMode(policy.getPolicyMode());
		policyTO.setStartDate(policy.getStartDate());
		return policyTO;
	}
	
	@Override
	public boolean addCustomer(CustomerTO customerTO) {
		
		if(customerRepository.save(getCustomer(customerTO)) !=null) 
		return true;
		else 
			return false;
	}
	
	@Override
	public boolean addPolicy(PolicyTO policyTO) {
		
		if(policyRepository.save(getPolicy(policyTO)) != null)
			return true;
		else 
			return false;	
	}

	
	@Override
	public boolean addCustomerWithPolicy(CustomerTO customerTO, PolicyTO policyTO) {
		
		Customer customer = getCustomer(customerTO);
		Policy policy = getPolicy(policyTO);
		
		System.out.println(customer);
		System.out.println(policy);
		
		Set<Policy> policies= customer.getPolicies();
		if(policies ==null) {
			policies= new HashSet<Policy>();
		}
		policies.add(policy);
		customer.setPolicies(policies);
		policy.setCustomer(customer);
		
		customerRepository.save(customer);
		return true;
	}
	
	@Override
	public PolicyTO getInsurancePolicy(long policyno) {
		Optional<Policy> optPolicy = policyRepository.findById(policyno);
		if(optPolicy.isPresent()) {
			return getPolicyTO(optPolicy.get());
		}
		return null;
	}
	
	@Override
	public Customer getCustomerDetails(long policyno) {
		Optional<Policy> optionalPolicy = policyRepository.findById(policyno);
		if(optionalPolicy.isPresent()) {
			Policy policy =optionalPolicy.get();
			System.out.println(optionalPolicy);
			return policy.getCustomer();
		}
		return null;
	}

}
