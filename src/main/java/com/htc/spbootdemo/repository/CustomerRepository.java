package com.htc.spbootdemo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.htc.spbootdemo.model.Customer;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, String>{

}
