package com.htc.spbootdemo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.htc.spbootdemo.model.Policy;

@Repository
public interface PolicyRepository extends CrudRepository<Policy, Long>{

}
