package com.htc.spbootdemo.to;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class CustomerTO {

	@NotEmpty(message="Customer code is mandatory")
	private String customerCode;
	@NotEmpty(message="Customer name cannot be empty")
	private String customerName;
	@NotEmpty(message="Address is mandatory")
	@Size(min=10, max=50, message="Address must be between 10 to 50 characters")
	private String address;
	@NotEmpty(message="Phoeno cannot be empty")
	@Size(min=10, max=10, message="Invalid Phoneno")
	private String contactno;
	
	public CustomerTO() {}

	public CustomerTO(String customerCode, String customerName, String address, String contactno) {
		super();
		this.customerCode = customerCode;
		this.customerName = customerName;
		this.address = address;
		this.contactno = contactno;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContactno() {
		return contactno;
	}

	public void setContactno(String contactno) {
		this.contactno = contactno;
	}

	@Override
	public String toString() {
		return "CustomerTO [customerCode=" + customerCode + ", customerName=" + customerName + ", address=" + address
				+ ", contactno=" + contactno + "]";
	}
	
}
