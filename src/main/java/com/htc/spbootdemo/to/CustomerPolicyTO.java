package com.htc.spbootdemo.to;

import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

public class CustomerPolicyTO {

	@NotEmpty(message="Customer code is mandatory")
	private String customerCode;
	@NotEmpty(message="Customer name cannot be empty")
	private String customerName;
	@NotEmpty(message="Address is mandatory")
	@Size(min=10, max=50, message="Address must be between 10 to 50 characters")
	private String address;
	@NotEmpty(message="Phone No cannot be empty")
	@Size(min=10, max=10, message="Invalid Phoneno")
	
	private String contactno;
	private long policyno;
	private double premium;
	private String policyMode;
	@DateTimeFormat(pattern="dd-MM-yyyy")
	private Date startDate;
	
	public CustomerPolicyTO() {
		
	}

	public CustomerPolicyTO(@NotEmpty(message = "Customer code is mandatory") String customerCode,
			@NotEmpty(message = "Customer name cannot be empty") String customerName,
			@NotEmpty(message = "Address is mandatory") @Size(min = 10, max = 50, message = "Address must be between 10 to 50 characters") String address,
			@NotEmpty(message = "Phoeno cannot be empty") @Size(min = 10, max = 10, message = "Invalid Phoneno") String contactno,
			long policyno, double premium, String policyMode, Date startDate) {
		super();
		this.customerCode = customerCode;
		this.customerName = customerName;
		this.address = address;
		this.contactno = contactno;
		this.policyno = policyno;
		this.premium = premium;
		this.policyMode = policyMode;
		this.startDate = startDate;
	}

	public String getCustomerCode() {
		return customerCode;
	}

	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContactno() {
		return contactno;
	}

	public void setContactno(String contactno) {
		this.contactno = contactno;
	}

	public long getPolicyno() {
		return policyno;
	}

	public void setPolicyno(long policyno) {
		this.policyno = policyno;
	}

	public double getPremium() {
		return premium;
	}

	public void setPremium(double premium) {
		this.premium = premium;
	}

	public String getPolicyMode() {
		return policyMode;
	}

	public void setPolicyMode(String policyMode) {
		this.policyMode = policyMode;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@Override
	public String toString() {
		return "CustomerPolicyTO [customerCode=" + customerCode + ", customerName=" + customerName + ", address="
				+ address + ", contactno=" + contactno + ", policyno=" + policyno + ", premium=" + premium
				+ ", policyMode=" + policyMode + ", startDate=" + startDate + "]";
	}
	
}
