package com.htc.spbootdemo.to;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

public class PolicyTO {

	private long policyno;
	private double premium;
	private String policyMode;
	private Date startDate;
	
	
	public PolicyTO() {}
	public PolicyTO(long policyno, double premium, String policyMode, Date startDate) {
		super();
		this.policyno = policyno;
		this.premium = premium;
		this.policyMode = policyMode;
		this.startDate = startDate;
	}
	public long getPolicyno() {
		return policyno;
	}
	public void setPolicyno(long policyno) {
		this.policyno = policyno;
	}
	public double getPremium() {
		return premium;
	}
	public void setPremium(double premium) {
		this.premium = premium;
	}
	public String getPolicyMode() {
		return policyMode;
	}
	public void setPolicyMode(String policyMode) {
		this.policyMode = policyMode;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	@Override
	public String toString() {
		return "PolicyTO [policyno=" + policyno + ", premium=" + premium + ", policyMode=" + policyMode + ", startDate="
				+ startDate + "]";
	}
	
	
}
